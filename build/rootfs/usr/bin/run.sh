#!/bin/sh
#!/bin/sh
addgroup -g ${GID} torrent && adduser -h /home/torrent -s /bin/sh -G torrent -D -u ${UID} torrent

mkdir -p /data/torrents
mkdir -p /data/.watch
mkdir -p /data/.session
mkdir -p /data/Media/Movies
mkdir -p /data/Media/TV\ Shows
mkdir -p /data/Media/Kids\ TV\ Shows
mkdir -p /data/Media/Kids\ Movies
mkdir /tmp/fastcgi /tmp/scgi /tmp/client_body

if [ $WEBROOT != "/" ]; then
    sed -i 's|<webroot>|'${WEBROOT}'|g' /sites/rutorrent.conf
    sed -i 's|<folder>||g' /sites/rutorrent.conf
    mv /var/www/torrent /var/www${WEBROOT}
else
    sed -i 's|<webroot>|/|g' /sites/rutorrent.conf
    sed -i 's|<folder>|/torrent|g' /sites/rutorrent.conf
fi

sed -i -e 's#<FILEBOT_RENAME_MOVIES>#'"$FILEBOT_RENAME_MOVIES"'#' \
       -e 's#<FILEBOT_RENAME_METHOD>#'"$FILEBOT_RENAME_METHOD"'#' \
       -e 's#<FILEBOT_RENAME_MUSICS>#'"$FILEBOT_RENAME_MUSICS"'#' \
       -e 's#<FILEBOT_RENAME_SERIES>#'"$FILEBOT_RENAME_SERIES"'#' \
       -e 's#<FILEBOT_RENAME_ANIMES>#'"$FILEBOT_RENAME_ANIMES"'#' /usr/bin/postdl

if [ ${RTORRENT_SCGI} -ne 0 ]; then
    sed -i -e 's|^scgi_local.*$|scgi_port = 0.0.0.0:'${RTORRENT_SCGI}'|' /home/torrent/.rtorrent.rc
    sed -i -e 's|socket: true,|socket: false,|' -e 's|port: 5000,|port: '${RTORRENT_SCGI}',|' /usr/flood/config.js
fi

chown -R torrent:torrent /var/www /var/lib/nginx /filebot /tmp /usr/flood /flood-db /etc/s6.d
rm -f /data/.session/rtorrent.lock

/usr/bin/supervisord -c /etc/supervisord.conf

exec su-exec $UID:$GID /bin/s6-svscan /etc/s6.d
